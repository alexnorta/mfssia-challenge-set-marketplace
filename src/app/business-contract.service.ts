import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { ProductContract } from './product-contract';



@Injectable({
  providedIn: 'root'
})
export class BusinessContractService {

  constructor(private http: HttpClient) { }

  url: string = "https://api.mfssia.tk/api/contract/product/publish";

   addContract(contract: ProductContract): Observable<any> {
      const headers = {
        'Content-Type': 'application/json'//,
        //'accept': '*/*',
        //"Access-Control-Allow-Origin": "*",
        //'Access-Control-Allow-Headers': 'Content-Type, Authorization',
        //'Access-Control-Allow-Methods': '*'
      }

      const body=JSON.stringify(contract);
      console.log(body)
      return this.http.post(this.url, body,{'headers':headers})
    }

    nodeInfo(): Observable<any> {
      const headers = {
        'content-type': 'application/json',
        'accept': '*/*',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Headers': 'Content-Type, Authorization, Accept',
        'Access-Control-Allow-Methods': '*'}

        return this.http.get("https://api.mfssia.tk/api/setup/node-info");
    }
}
