import {LogisticBusinessObligation} from './logistic-business-obligation';
import {M2XObligation} from './m2x-obligation';
import {ProductPurchaseObligation} from './product-purchase-obligation';
import {ServiceDeliveryObligation} from './service-delivery-obligation';

export interface Contract {
  contractNumber: string;
  actor1Name: string;
  actor1Role: string;
  actor1Identification: string;
  actor1Network: string;
  actor2Name: string;
  actor2Role: string;
  actor2Identification: string;
  actor2Network: string;
  contractBeginDate: string;
  contractEndDate: string;
  contractType: string;
  logisticBusinessObligation: LogisticBusinessObligation;
  m2xObligation: M2XObligation;
  productPurchaseObligation: ProductPurchaseObligation;
  serviceDeliveryObligation: ServiceDeliveryObligation;
}
