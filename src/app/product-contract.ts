export interface ProductContract {
  contractNo: string;
  consumerNetwork: string;
  producerNetwork: string;
  deliveryInterval: string;
  price: string;
  quantity: string;
  timestamp: string;
  productName: string;
  uuid: string;
}