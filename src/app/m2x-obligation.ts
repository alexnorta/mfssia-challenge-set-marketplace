export interface M2XObligation {
  devices: string;
  dataType: string;
  protocol: string;
}
