export interface LogisticBusinessObligation {
  productName: string;
  deliveryPrice: string;
  quantity: string;
  productOrigin: string;
  productDestination: string;
}
