export interface ProductPurchaseObligation {
  productName: string;
  productPrice: string;
  productQuantity: string;
}
