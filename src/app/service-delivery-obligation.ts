export interface ServiceDeliveryObligation {
  servicePrice: string;
  serviceType: string;
}
