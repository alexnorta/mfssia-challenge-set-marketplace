import { TestBed } from '@angular/core/testing';

import { BusinessContractService } from './business-contract.service';

describe('BusinessContractService', () => {
  let service: BusinessContractService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BusinessContractService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
