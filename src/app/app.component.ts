import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {BusinessContractService} from './business-contract.service';
import {ProductContract} from './product-contract';
import * as uuid from 'uuid';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit{
  title = 'newMat';
  isLinear = true;
  actorsFormGroup!: FormGroup;
  contractTypeFormGroup!: FormGroup;
  serviceDeliveryFormGroup!: FormGroup;
  m2xFormGroup!: FormGroup;
  productPurchaseFormGroup!: FormGroup;
  logisticBusinessFormGroup!: FormGroup;
  dkgHandler!: string;
  nodeVersion!: string;

  constructor(
    private _formBuilder: FormBuilder,
    private _businessContractService: BusinessContractService) {
      this._businessContractService.nodeInfo().subscribe(data => {
        this.nodeVersion = data.version
      });
    }

  ngOnInit() {
      this.actorsFormGroup = this._formBuilder.group({
        actor1_name: ['', Validators.required],
        actor1_role: [''],
        actor1_identification: [''],
        actor1_network: ['', Validators.required],
        actor2_name: ['', Validators.required],
        actor2_role: [''],
        actor2_identification: [''],
        actor2_network: ['', Validators.required],
        contract_begin_date: [''],
        contract_end_date: [''],
        contractNumber: ['', Validators.required],
        deliveryInterval: ['', Validators.required],
        dkgHandler: ['']

      });

      this.contractTypeFormGroup = this._formBuilder.group({
        contract_type: ['', Validators.required]
      });

      this.serviceDeliveryFormGroup = this._formBuilder.group({
        service_type: ['', Validators.required],
        service_price: ['', Validators.required]
      });

      this.m2xFormGroup = this._formBuilder.group({
        devices: ['', Validators.required],
        data_type: ['', Validators.required],
        protocol: ['', Validators.required],
      });

      this.productPurchaseFormGroup = this._formBuilder.group({
        product_name: ['', Validators.required],
        product_price: ['', Validators.required],
        product_quantity: ['', Validators.required]
      });

      this.logisticBusinessFormGroup = this._formBuilder.group({
        product_name: ['', Validators.required],
        quantity: ['', Validators.required],
        delivery_price: ['', Validators.required],
        product_origin: ['', Validators.required],
        product_destination: ['', Validators.required]
      });
  }

  submit(){
    console.log(this.actorsFormGroup.value);
    console.log(this.contractTypeFormGroup.value);

    const contract: ProductContract = {
      contractNo: this.actorsFormGroup.value.contractNumber,
      producerNetwork: this.actorsFormGroup.value.actor1_network,
      consumerNetwork: this.actorsFormGroup.value.actor2_network,
      deliveryInterval: this.actorsFormGroup.value.deliveryInterval,
      price: this.productPurchaseFormGroup.value.product_price,
      quantity: this.productPurchaseFormGroup.value.product_quantity,
      productName: this.productPurchaseFormGroup.value.product_name,
      uuid: uuid.v4(),
      timestamp: new Date().toString()
    }

    this._businessContractService.addContract(contract)
      .subscribe(data => {
          this.dkgHandler = data
      });
  }
}
